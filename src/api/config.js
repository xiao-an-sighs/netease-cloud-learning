let baseURL = ''
if (process.env.NODE_ENV === 'development') {
  baseURL = 'http://localhost:3000'
} else {
  baseURL = 'https://nicemusic-api.lxhcool.cn/'
}
const time = 1000 * 60
export default { baseURL, time }
