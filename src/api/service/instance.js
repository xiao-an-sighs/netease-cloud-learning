import axios from 'axios'
import config from '../config'
import { Message } from 'element-ui'
import router from '@/router'
const { baseURL, time } = config
const instance = axios.create({
  baseURL,
  time
})
// 请求拦截器
instance.interceptors.response.use(response => {
  const data = response.data
  const status = response.status
  if (status === 200) {
    // return Promise.resolve(data)
    return data
  } else if (status === 301) {
    Message.error({ message: '请先登录' })
    router.replace({
      path: 'login'
    })
    return
  } else {
    return Promise.reject(data)
  }
})
export default instance
