import instance from './instance'

/**
 * @method 获取推荐歌单
 * @params limit 取出数量默认为 30
 */
function getHotRecomment(data) {
  return instance({
    url: `/personalized?limit=${data}`,
    method: 'get'
  })
}

export default { getHotRecomment }
