import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Cookies from 'js-cookie'
import Element from 'element-ui'
import api from './api/service/api'
import utils from '@/utils'
import 'element-ui/lib/theme-chalk/index.css'

import '@/assets/public.css'

Vue.config.productionTip = false

Vue.prototype.$api = api
Vue.prototype.$utils = utils
Vue.use(Element, {
  size: Cookies.get('size') || 'mini' // set element-ui default size
})
new Vue({
  router,
  store,
  render: (h) => h(App)
}).$mount('#app')
